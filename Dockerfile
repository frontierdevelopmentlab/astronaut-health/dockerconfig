#############################
# FDL US 2021 - Astronaut Health Team
# 2021 Docker Version:v07
# Paul Duckworth, Linus Scheibenreif & Frank Soboczenski
# 2021, July
#############################

FROM nvidia/cuda:11.4.0-base-ubuntu20.04

LABEL authors="frank.soboczenski@gmail.com, pduckworth@oxfordrobotics.institute, scheibenreif.linus@gmail.com"

# Install some basic utilities
RUN apt-get update && apt-get install -y \
    curl \
    ca-certificates \
    sudo \
    bash \
    git \
    bzip2 \
    libx11-6 \
 && rm -rf /var/lib/apt/lists/*

# Create a working directory
RUN mkdir /app
COPY . /app
WORKDIR /app

# Create a non-root user and switch to it
RUN adduser --disabled-password --gecos '' --shell /bin/bash user \
 && chown -R user:user /app
RUN echo "user ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/90-user
USER user

# All users can use /home/user as their home directory

ENV HOME=/home/user
RUN chmod 777 /home/user

#RUN git clone --single-branch --branch fdl2021 https://github.com/PDuckworth/openfl.git
#RUN git clone https://github.com/GoogleCloudPlatform/gcsfuse.git
# RUN git clone --single-branch --branch federated_lineaer_reg https://gitlab.com/frontierdevelopmentlab/astronaut-health/crisp.git

## Add the GCP Bucket "ah21_data" to the home directory
#RUN sudo mkdir /home/data
#RUN cd /home/user/gcsfuse
#RUN gcsfuse --implicit-dirs -only-dir Paul ah21_data /home/data

#RUN export PYTHONPATH=/home/user/openfl:$PYTHONPATH
#RUN export PYTHONPATH=/home/user/gcsfuse:$PYTHONPATH


# Install Miniconda and Python 3.8
ENV CONDA_AUTO_UPDATE_CONDA=false
ENV PATH=/home/user/miniconda/bin:$PATH
RUN curl -sLo ~/miniconda.sh https://repo.continuum.io/miniconda/Miniconda3-py38_4.9.2-Linux-x86_64.sh \
 && chmod +x ~/miniconda.sh \
 && ~/miniconda.sh -b -p ~/miniconda \
 && rm ~/miniconda.sh \
 && conda install -y python==3.8.3\
 && conda clean -ya \
 && conda update conda

# CUDA 11.1-specific steps
RUN conda install -y -c conda-forge cudatoolkit=11.1.1 \
 && conda install -y -c pytorch \
     "pytorch=1.8.1=py3.8_cuda11.1_cudnn8.0.5_0" \
     "torchvision=0.9.1=py38_cu111" \
 && conda clean -ya

# Installing neccessary libraries
RUN pip install -r requirements.txt
#RUN sh -c 'echo -e IMAGE COMPLETED - READY TO RUN'

# Set the default command to python3
CMD ["python3"]
CMD tail -f /dev/null

